package com.rathravane.till.data.csv;

import java.util.Date;

public class CsvLineBuilder 
{
	public CsvLineBuilder ()
	{
		this ( '"', ',', true );
	}

	public CsvLineBuilder ( char quoteChar, char sepChar, boolean forceQuotes )
	{
		fBuilder = new StringBuilder ();
		fDoneOne = false;
		fQuoteChar = quoteChar;
		fSepChar = sepChar;
		fForceQuotes = forceQuotes;
	}

	@Override
	public String toString ()
	{
		return fBuilder.toString ();
	}

	public CsvLineBuilder append ( String value )
	{
		return appendLiteral ( csvEncoder.encodeForCsv ( value, fQuoteChar, fSepChar, fForceQuotes ) );
	}

	public CsvLineBuilder append ( long value )
	{
		return appendLiteral ( Long.toString ( value ) );
	}

	public CsvLineBuilder append ( boolean value )
	{
		return appendLiteral ( Boolean.toString ( value ) );
	}

	public CsvLineBuilder append ( double value )
	{
		return appendLiteral ( Double.toString ( value ) );
	}

	public CsvLineBuilder append ( Date value )
	{
		return appendLiteral ( csvEncoder.encodeForCsv ( value ) );
	}

	private final StringBuilder fBuilder;
	private final char fQuoteChar;
	private final char fSepChar;
	private final boolean fForceQuotes;
	private boolean fDoneOne;

	public CsvLineBuilder appendLiteral ( String val )
	{
		if ( fDoneOne ) fBuilder.append ( ',' );
		fBuilder.append ( val );
		fDoneOne = true;
		return this;
	}
}
