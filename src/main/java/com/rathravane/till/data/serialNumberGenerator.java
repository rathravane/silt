package com.rathravane.till.data;

/**
 * Generate serial numbers.
 * @author peter
 *
 */
public class serialNumberGenerator
{
	public serialNumberGenerator ()
	{
		this ( "" );
	}

	public serialNumberGenerator ( String prefix )
	{
		this ( prefix, 1 );
	}

	public serialNumberGenerator ( long startAt )
	{
		this ( "", startAt );
	}

	public serialNumberGenerator ( String prefix, long startAt )
	{
		fNext = startAt;
		fPrefix = prefix;
	}

	public synchronized String getNext ()
	{
		return fPrefix + (fNext++);
	}

	private final String fPrefix;
	private long fNext;
}
