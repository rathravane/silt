package com.rathravane.till.data.exprEval;


public interface ExprDataSource
{
	/**
	 * get the value of an object given a label
	 * @param label
	 * @return a data object
	 */
	Object eval ( String label );
}
