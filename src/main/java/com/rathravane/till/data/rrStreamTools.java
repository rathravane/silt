package com.rathravane.till.data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class rrStreamTools
{
	public static class StreamCopier
	{
		public StreamCopier from ( InputStream is )
		{
			fFrom = is;
			return this;
		}

		public StreamCopier to ( OutputStream os )
		{
			fTo = os;
			return this;
		}

		public StreamCopier withBufferSize ( int bufSize )
		{
			fBufferSize = bufSize;
			return this;
		}
		
		public StreamCopier closeStream () { return closeStream ( true ); }
		public StreamCopier leaveStreamOpen () { return closeStream ( false ); }

		public StreamCopier closeStream ( boolean cs )
		{
			fCloseStream = cs;
			return this;
		}

		public void copy () throws IOException
		{
			final byte[] buffer = new byte [fBufferSize];
			int len;
			while ( ( len = fFrom.read ( buffer ) ) != -1 )
			{
				fTo.write ( buffer, 0, len );
			}
			if ( fCloseStream ) fTo.close ();
		}

		private InputStream fFrom;
		private OutputStream fTo;
		private int fBufferSize = kBufferLength;
		private boolean fCloseStream = true;
	}
	
	protected static final int kBufferLength = 4096;

	/**
	 * Reads the stream into a byte array using a default-sized array for each read,
	 * then closes the input stream.
	 * 
	 * @param is
	 * @return a byte array
	 * @throws IOException
	 */
	public static byte[] readBytes ( InputStream is ) throws IOException
	{
		return readBytes ( is, kBufferLength );
	}

	/**
	 * Reads the stream into a byte array using a bufferSize array for each read,
	 * then closes the input stream.
	 * 
	 * @param is
	 * @param bufferSize
	 * @return a byte array
	 * @throws IOException
	 */
	public static byte[] readBytes ( InputStream is, int bufferSize ) throws IOException
	{
		return readBytes ( is, bufferSize, -1 );
	}

	/**
	 * Reads the stream into a byte array using a bufferSize array for each read,
	 * then closes the input stream. If limit &gt;= 0, at most limit bytes are read.<br>
	 * Note: even with a negative limit, 2GB is the limit.
	 * 
	 * @param is
	 * @param bufferSize
	 * @param limit
	 * @return a byte array
	 * @throws IOException
	 */
	public static byte[] readBytes ( InputStream is, int bufferSize, int limit ) throws IOException
	{
		int counter = 0;
		final int atMost = limit < 0 ? Integer.MAX_VALUE : Math.min ( limit, Integer.MAX_VALUE );

		final ByteArrayOutputStream baos = new ByteArrayOutputStream ();
		if ( is != null )
		{
			byte[] b = new byte [ bufferSize ];
			int len = 0;
			do
			{
				len = is.read ( b );
				if ( -1 != len )
				{
					final int readNow = Math.min ( len, atMost - counter );
					baos.write ( b, 0, readNow );
					counter += readNow;
				}
			}
			while ( len != -1 && counter < atMost );
			is.close ();
		}

		return baos.toByteArray ();
	}

	/**
	 * Copy from the input stream to the output stream, then close the output stream.
	 * @param in
	 * @param out
	 * @throws IOException
	 */
	public static void copyStream ( InputStream in, OutputStream out ) throws IOException
	{
		new StreamCopier().from ( in ).to ( out ).copy ();
	}

	/**
	 * Copy from the input stream to the output stream, then close the output stream.
	 * @param in
	 * @param out
	 * @param bufferSize
	 * @throws IOException
	 */
	public static void copyStream ( InputStream in, OutputStream out, int bufferSize ) throws IOException
	{
		new StreamCopier().from ( in ).to ( out ).withBufferSize ( bufferSize ).copy ();
	}


	/**
	 * Copy from the input stream to the output stream, then close the output stream.
	 * @param in
	 * @param out
	 * @param bufferSize
	 * @param closeOutputStream
	 * @throws IOException
	 */
	public static void copyStream ( InputStream in, OutputStream out, int bufferSize, boolean closeOutputStream ) throws IOException
	{
		new StreamCopier().from ( in ).to ( out ).withBufferSize ( bufferSize ).closeStream ( closeOutputStream ).copy ();
	}
}
