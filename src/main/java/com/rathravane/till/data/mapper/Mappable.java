package com.rathravane.till.data.mapper;

public interface Mappable<E>
{
	E getType ();
}
