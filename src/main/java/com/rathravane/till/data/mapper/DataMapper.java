package com.rathravane.till.data.mapper;

import java.util.HashMap;

public class DataMapper<E1, T1 extends Mappable<E1>, E2, T2 extends Mappable<E2>>
{
	public static class UnsupportedValueType extends Exception
	{
		public UnsupportedValueType ( String msg ) { super ( msg ); }
		private static final long serialVersionUID = 1L;
	}
	
	public interface valIo<T1,T2>
	{
		T2 translate ( T1 v ) throws UnsupportedValueType;
	}

	public void registerForward ( E1 fromType, valIo<T1,T2> vio )
	{
		forwardMap.put ( fromType, vio );
	}

	public void registerBack ( E2 toType, valIo<T2,T1> vio )
	{
		reverseMap.put ( toType, vio );
	}

	public T2 translateForward ( T1 val ) throws UnsupportedValueType
	{
		final E1 fromType = val.getType ();
		final valIo<T1,T2> io = forwardMap.get ( fromType );
		if ( io == null )
		{
			throw new UnsupportedValueType ( "No writer for type [" + fromType.toString () + "]." );
		}
		return io.translate ( val );
	}
	
	public T1 translateBack ( T2 val ) throws UnsupportedValueType
	{
		final E2 fromType = val.getType ();
		final valIo<T2,T1> io = reverseMap.get ( fromType );
		if ( io == null )
		{
			throw new UnsupportedValueType ( "No reader for type [" + fromType.toString () + "]." );
		}
		return io.translate ( val );
	}

	private final HashMap<E1,valIo<T1,T2>> forwardMap = new HashMap<E1,valIo<T1,T2>> ();
	private final HashMap<E2,valIo<T2,T1>> reverseMap = new HashMap<E2,valIo<T2,T1>> ();
}
