package com.rathravane.till.data.json;

import java.io.InputStream;
import java.io.Reader;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rathravane.till.data.json.JsonVisitor.ArrayOfObjectVisitor;
import com.rathravane.till.data.json.JsonVisitor.ArrayOfStringVisitor;

public class JsonUtil
{
	public static JSONObject readJsonObject ( InputStream is )
	{
		return new JSONObject ( new CommentedJsonTokener ( is ) );
	}
	
	public static JSONObject readJsonObject ( String is )
	{
		return new JSONObject ( new CommentedJsonTokener ( is ) );
	}

	public static JSONObject readJsonObject ( Reader is )
	{
		return new JSONObject ( new CommentedJsonTokener ( is ) );
	}

	public static JSONArray readJsonArray ( InputStream is )
	{
		return new JSONArray ( new CommentedJsonTokener ( is ) );
	}

	public static JSONArray readJsonArray ( String is )
	{
		return new JSONArray ( new CommentedJsonTokener ( is ) );
	}

	public static JSONArray readJsonArray ( Reader is )
	{
		return new JSONArray ( new CommentedJsonTokener ( is ) );
	}

	public static JSONObject clone ( JSONObject that )
	{
		if ( that == null ) return null;
		return (JSONObject) cloneJsonValue ( that );
	}

	public static Object cloneJsonValue ( Object value )
	{
		if ( value == null ) return null;

		if ( value instanceof JSONObject )
		{
			final JSONObject result = new JSONObject ();
			final JSONObject obj = (JSONObject) value;
			for ( Object key : obj.keySet () )
			{
				final String keyStr = key.toString ();
				result.put ( keyStr, cloneJsonValue ( obj.get ( keyStr ) ) );
			}
			return result;
		}
		else if ( value instanceof JSONArray )
		{
			final JSONArray array = (JSONArray) value;
			final JSONArray result = new JSONArray ();
			for ( int i=0; i<array.length(); i++ )
			{
				result.put ( i, cloneJsonValue ( array.get ( i ) ) );
			}
			return result;
		}
		else
		{
			return value;
		}
	}
	
	public static void copyInto ( JSONObject src, final JSONObject dest )
	{
		if ( src == null || dest == null ) return;
		for ( Object key : src.keySet () )
		{
			final String keyStr = key.toString ();
			dest.put ( keyStr, cloneJsonValue ( src.get ( keyStr ) ) );
		}
	}

	public static int getIndexOfStringInArray ( String s, JSONArray a )
	{
		if ( a == null || s == null ) return -1;

		int found = -1;
		for ( int i=0; i<a.length (); i++ )
		{
			if ( s.equals ( a.getString ( i ) ) )
			{
				found = i;
				break;
			}
		}

		return found;
	}

	public static boolean ensureStringInArray ( String s, JSONArray a )
	{
		if ( a == null || s == null ) return false;

		final int found = getIndexOfStringInArray ( s, a );
		if ( found < 0 )
		{
			a.put ( s );
			return true;
		}

		return false;
	}

	public static boolean removeStringFromArray ( JSONArray a, String s )
	{
		if ( a == null || s == null ) return false;

		final int found = getIndexOfStringInArray ( s, a );
		if ( found > -1 )
		{
			a.remove ( found );
			return true;
		}

		return false;
	}

	/**
	 * Load a string or array of strings into a string list.
	 * 
	 * @param base the base json object
	 * @param key the key of the string array
	 * @return a list of 0 or more strings
	 * @throws JSONException if the key doesn't exist, or if it's not a string or array of strings
	 */
	public static List<String> readStringArray ( JSONObject base, String key ) throws JSONException
	{
		final LinkedList<String> result = new LinkedList<String> ();

		final Object oo = base.opt ( key );
		if ( oo == null ) throw new JSONException ( key + " does not exist" );

		if ( oo instanceof JSONArray )
		{
			JsonVisitor.forEachStringElement ( (JSONArray)oo, new ArrayOfStringVisitor () 
			{
				@Override
				public boolean visit ( String str ) throws JSONException
				{
					result.add ( str );
					return true;
				}
			} );
		}
		else
		{
			result.add ( oo.toString () );
		}

		return result;
	}

	/**
	 * Sort a JSON array of JSON objects using the value retrieved by the value expression.
	 * @param a
	 * @param valueExpression
	 */
	public static void sortArrayOfObjects ( final JSONArray a, final String valueExpression )
	{
		sortArrayOfObjects ( a, valueExpression, false );
	}

	/**
	 * Sort a JSON array of JSON objects using the value retrieved by the value expression.
	 * @param a
	 * @param valueExpression
	 * @param reverse
	 */
	public static void sortArrayOfObjects ( final JSONArray a, final String valueExpression, boolean reverse )
	{
		// build a list of objects
		final LinkedList<JSONObject> list = new LinkedList<JSONObject> ();
		JsonVisitor.forEachObjectIn ( a, new ArrayOfObjectVisitor ()
		{
			@Override
			public boolean visit ( JSONObject t ) throws JSONException
			{
				if ( t != null )
				{
					list.add ( t );
				}
				return true;
			}
		});

		// sort the list
		Collections.sort ( list, new Comparator<JSONObject> () {

			@Override
			public int compare ( JSONObject o1, JSONObject o2 )
			{
				final Object oval1 = JsonEval.eval ( o1, valueExpression );
				final Object oval2 = JsonEval.eval ( o2, valueExpression );
				if (( oval1 instanceof Long ) && ( oval2 instanceof Long ))
				{
					final Long n1 = (Long) oval1;
					final Long n2 = (Long) oval2;
					return n1.compareTo ( n2 );
				}
				if (( oval1 instanceof Integer ) && ( oval2 instanceof Integer ))
				{
					final Integer n1 = (Integer) oval1;
					final Integer n2 = (Integer) oval2;
					return n1.compareTo ( n2 );
				}

				final String e1 = oval1 == null ? "" : oval1.toString ();
				final String e2 = oval2 == null ? "" : oval2.toString ();
				return e1.compareTo ( e2 );
			}
		} );
		if ( reverse )
		{
			Collections.reverse ( list );
		}

		// rewrite the array
		int index = 0;
		for ( JSONObject o : list )
		{
			a.put ( index++, o );
		}
	}
}
