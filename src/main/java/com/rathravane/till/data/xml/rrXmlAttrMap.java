package com.rathravane.till.data.xml;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class rrXmlAttrMap implements Map<String,String>
{
	public rrXmlAttrMap ()
	{
		this ( null );
	}

	public rrXmlAttrMap ( rrXmlAttrMap under )
	{
		fMap = new HashMap<String,String> ();
	}

	public <T extends Exception> String getOrThrow ( String key, T e ) throws T
	{
		String result = get ( key );
		if ( result == null )
		{
			throw e;
		}
		return result;
	}
	
	public void clear () { fMap.clear (); }
	public boolean containsKey ( Object k ) { return fMap.containsKey ( k ); }
	public boolean containsValue ( Object k ) { return fMap.containsValue ( k ); }
	public Set<java.util.Map.Entry<String, String>> entrySet () { return fMap.entrySet (); }
	public String get ( Object arg0 ) { return fMap.get ( arg0 ); }
	public boolean isEmpty () { return fMap.isEmpty (); }
	public Set<String> keySet () { return fMap.keySet (); }
	public String put ( String arg0, String arg1 ) { return fMap.put ( arg0, arg1); }
	public void putAll ( Map<? extends String, ? extends String> arg0 ) { fMap.putAll ( arg0 ); }
	public String remove ( Object arg0 ) { return fMap.remove ( arg0 ); }
	public int size () { return fMap.size(); }
	public Collection<String> values () { return fMap.values (); }

	private final HashMap<String,String> fMap;
}
