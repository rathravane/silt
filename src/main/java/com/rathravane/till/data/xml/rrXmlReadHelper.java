package com.rathravane.till.data.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Utilities for reading XML streams.
 * @author peter
 *
 */
public class rrXmlReadHelper
{
	public static final String kTextContent = "_textContent";

	public static Element getTopLevelElement ( byte[] docBytes )
	{
		final InputStream is = new ByteArrayInputStream ( docBytes );
		return getTopLevelElement ( is );
	}

	public static Element getTopLevelElement ( InputStream is )
	{
		Element result = null;
		try
		{
			final DocumentBuilderFactory f = DocumentBuilderFactory.newInstance ();
			f.setValidating ( false );
			f.setNamespaceAware ( false );

			final DocumentBuilder db = f.newDocumentBuilder ();
			db.setEntityResolver ( new EntityResolver ()
			{
				@Override
				public InputSource resolveEntity ( String publicId, String systemId )
				{
					// it might be a good idea to insert a trace logging here
					// that you are ignoring publicId/systemId
					return new InputSource ( new StringReader ( "" ) );
				}
			} );

			final Document d = db.parse ( is );
			result = d.getDocumentElement ();
		}
		catch ( ParserConfigurationException e )
		{
			log.warning ( "XML parse error: " + e.getMessage () );
		}
		catch ( SAXException e )
		{
			log.warning ( "XML parse error: " + e.getMessage () );
		}
		catch ( IOException e )
		{
			log.warning ( "XML parse error: " + e.getMessage () );
		}
		return result;
	}

	public static Element getFirstNamedChildElement ( Element parent, String name )
	{
		Element result = null;
		Collection<Element> elements = getNamedChildElements ( parent, name );
		if ( elements.size () > 0 )
		{
			result = elements.iterator ().next ();
		}
		return result;
	}

	public static Node getFirstNamedChildNode ( Node parent, String name )
	{
		Node result = null;
		Collection<Node> elements = getNamedChildNodes ( parent, name );
		if ( elements.size () > 0 )
		{
			result = elements.iterator ().next ();
		}
		return result;
	}

	public static Collection<Element> getNamedChildElements ( Element parent, String name )
	{
		final LinkedList<Element> result = new LinkedList<Element> ();
		final NodeList elements = parent.getElementsByTagName ( name );
		for ( int i=0; i<elements.getLength(); i++ )
		{
			Node n = elements.item ( i );
			if ( n instanceof Element )
			{
				result.add ( (Element) n );
			}
		}
		return result;
	}

	public static Collection<Node> getNamedChildNodes ( Node parent, String name )
	{
		final LinkedList<Node> result = new LinkedList<Node> ();
		final NodeList nodes = parent.getChildNodes ();
		for ( int i=0; i<nodes.getLength(); i++ )
		{
			Node n = nodes.item ( i );
			if ( n.getNodeName ().equals ( name ) )
			{
				result.add ( n );
			}
		}
		return result;
	}

	public static Collection<Element> getAllChildElements ( Node parent )
	{
		LinkedList<Element> result = new LinkedList<Element> ();
		NodeList elements = parent.getChildNodes ();
		for ( int i=0; i<elements.getLength(); i++ )
		{
			Node n = elements.item ( i );
			if ( n instanceof Element )
			{
				result.add ( (Element) n );
			}
		}
		return result;
	}

	public static String getTextFromNamedChild ( Node parent, String childName )
	{
		return getTextFromNamedChild ( parent, childName, null );
	}

	public static byte[] getBytesFromNamedChild ( Node parent, String childName )
	{
		byte[] result = null;
		String text = getTextFromNamedChild ( parent, childName, null );
		if ( text != null )
		{
			result = text.getBytes ();
		}
		return result;
	}

	public static String getTextFromNamedChild ( Node parent, String childName, String defValue )
	{
		String result = defValue;
		NodeList children = parent.getChildNodes ();
		final int len = children.getLength ();
		for ( int i=0; result == null && i<len; i++ )
		{
			Node n = children.item ( i );
			if ( n != null )
			{
				final String nodename = n.getNodeName ();
				if ( nodename.equals ( childName ) )
				{
					result = n.getTextContent ();
				}
			}
		}
		return result;
	}

	public static long getLongFromNamedChild ( Node parent, String named, long defValue )
	{
		long result = defValue;
		String text = getTextFromNamedChild ( parent, named );
		try
		{
			result = Long.parseLong ( text );
		}
		catch ( NumberFormatException x )
		{
			result = defValue;
		}
		return result;
	}

	private static class attrEntry implements Entry<String,String>
	{
		public attrEntry ( String k, String v )
		{
			fKey = k;
			fValue = v;
		}

		public String getKey () { return fKey; }
		public String getValue () { return fValue; }
		public String setValue ( String arg0 ) throws UnsupportedOperationException { throw new UnsupportedOperationException(); }

		private final String fKey;
		private final String fValue;
	}

	public static rrXmlAttrMap getAttrMap ( Node n )
	{
		return getAttrMap ( n, null );
	}

	public static rrXmlAttrMap getAttrMap ( Node n, rrXmlAttrMap inherited )
	{
		return getAttrMap ( n, inherited, true );
	}

	public static rrXmlAttrMap getAttrMap ( Node n, rrXmlAttrMap inherited, boolean withTextContent )
	{
		final rrXmlAttrMap result = new rrXmlAttrMap ( inherited );
		for ( Entry<String,String> e : getAllAttrs ( n, withTextContent ) )
		{
			result.put ( e.getKey (), e.getValue () );
		}
		return result;
	}

	public static Collection<Entry<String,String>> getAllAttrs ( Node n, boolean withTextContent )
	{
		LinkedList<Entry<String,String>> result = new LinkedList<Entry<String,String>> ();

		NamedNodeMap nnm = n.getAttributes ();
		if ( nnm != null )
		{
			final int size = nnm.getLength ();
			for ( int i=0; i<size; i++ )
			{
				Node attr = nnm.item ( i );
				if ( attr != null )
				{
					result.add (
						new attrEntry ( attr.getNodeName (), attr.getNodeValue () ) );
				}
			}
		}

		if ( withTextContent )
		{
			result.add ( new attrEntry ( kTextContent, n.getTextContent() ) );
		}

		return result;
	}

	public static String getAttr ( Node n, String attrName, String defVal )
	{
		String result = defVal;
		NamedNodeMap nnm = n.getAttributes ();
		if ( nnm != null )
		{
			Node attr = nnm.getNamedItem ( attrName );
			if ( attr != null )
			{
				result = attr.getNodeValue ();
			}
		}
		return result;
	}

	public static int getIntAttr ( Node n, String attrName, int defVal )
	{
		int result = defVal;
		try
		{
			String textResult = getAttr ( n, attrName, "" + defVal );
			result = Integer.parseInt ( textResult );
		}
		catch ( NumberFormatException x )
		{
			result = defVal;
		}
		return result;
	}

	public interface attrAction <T extends Exception> { void onAttr ( Node parent, String name, String val ) throws T; };
	public static <T extends Exception> void forEachAttr ( Node n, final attrAction<T> aa ) throws T
	{
		for ( Entry<String,String> e : getAllAttrs ( n, false ) )
		{
			aa.onAttr ( n, e.getKey(), e.getValue () );
		}
	}

	public interface elementAction <T extends Exception> { void onElement ( Node parent, Element e ) throws T; };
	public static <T extends Exception> void forEachElement ( Node n, final elementAction<T> aa ) throws T
	{
		for ( Element e : getAllChildElements ( n ) )
		{
			aa.onElement ( n, e );
		}
	}

	public static <T extends Exception> void forEachElementNamed ( Node n, String name, final elementAction<T> aa ) throws T
	{
		for ( Element e : getAllChildElements ( n ) )
		{
			final String nodeName = e.getNodeName ();
			if ( nodeName.equals ( name ) )
			{
				aa.onElement ( n, e );
			}
		}
	}

	private static final Logger log = Logger.getLogger ( rrXmlReadHelper.class.getName () );
}
