/*
 *	Copyright 2006-2017, Rathravane LLC
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *	
 *	http://www.apache.org/licenses/LICENSE-2.0
 *	
 *	Unless required by applicable law or agreed to in writing, software
 *	distributed under the License is distributed on an "AS IS" BASIS,
 *	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	See the License for the specific language governing permissions and
 *	limitations under the License.
 */
package com.rathravane.till.legal;

public interface rrStdCopyrightInfo
{
	public static final String kHolder = "Rathravane LLC";
	public static final int kStartYear = 2004;

	// this must be updated each year, but it prevents the need for tracking
	// the date in every project.
	public static final int kBuildYear = 2018;
}
