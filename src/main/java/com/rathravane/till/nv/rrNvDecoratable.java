package com.rathravane.till.nv;

/**
 * A decoratable class is one that has decorations in a writeable
 * name/value table.
 * 
 * @author peter
 *
 */
public interface rrNvDecoratable
{
	public rrNvWriteable getDecorations ();
}
