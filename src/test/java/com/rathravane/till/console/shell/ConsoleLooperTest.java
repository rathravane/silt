package com.rathravane.till.console.shell;

import junit.framework.TestCase;

import org.junit.Test;

public class ConsoleLooperTest extends TestCase
{
	private static final String[][] kSplitTests =
	{
		new String[] { "foo", "foo" },
		new String[] { "a b", "a", "b" },
		new String[] { "a \"b c\" d", "a", "b c", "d" },
		new String[] { "a\"bc\"d", "a\"bc\"d" },
		new String[] { "{\"foo\":\"bar\"}", "{\"foo\":\"bar\"}" },
		new String[] { "\"{ 'foo': 'bar' }\"", "{ 'foo': 'bar' }" },
	};
	
	@Test
	public void testLineSplits ()
	{
		for ( String[] test : kSplitTests )
		{
			final String[] result = ConsoleLooper.splitLine ( test[0] );
			assertEquals ( test.length-1, result.length );
			for ( int i=0; i<result.length; i++ )
			{
				assertEquals ( test[i+1], result[i] );
			}
		}
	}
}
