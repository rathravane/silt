package com.rathravane.till.data.csv;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import junit.framework.TestCase;

import org.junit.Test;

import com.rathravane.till.data.csv.csvCallbackReader.recordHandler;

public class CsvCallbackReaderTest extends TestCase
{
	@Test
	public void testBrokenLine () throws IOException
	{
		final ByteArrayInputStream is = new ByteArrayInputStream (
			(
				"FieldA,FieldB\n" + 
				"\"a\",\"b\n" +
				"c\"\n" +
				"d,e"
			).getBytes () );

		final StringBuffer sb = new StringBuffer ();
		final csvCallbackReader reader = new csvCallbackReader ( true );
		reader.read ( is, new recordHandler()
		{
			@Override
			public boolean handler ( Map<String, String> fields )
			{
				sb.append ( fields.get ( "FieldA" ) );
				return true;
			}
		} );
		assertEquals ( "ad", sb.toString () );
	}

	@Test
	public void testBrokenHeaderLine () throws IOException
	{
		final ByteArrayInputStream is = new ByteArrayInputStream (
			(
				"FieldA,FieldB,\"Field\nC\"\n" +
				"\"a\",\"b\",\"c\"\n" +
				"\"d\",\"e\",\"f\"\n"
			).getBytes () );

		final StringBuffer sb = new StringBuffer ();
		final csvCallbackReader reader = new csvCallbackReader ( true );
		reader.read ( is, new recordHandler()
		{
			@Override
			public boolean handler ( Map<String, String> fields )
			{
				sb.append ( fields.get ( "FieldA" ) );
				return true;
			}
		} );
		assertEquals ( "ad", sb.toString () );
	}

	@Test
	public void testTermReader () throws IOException
	{
		for ( String[] test : terms )
		{
			final csvCallbackReader r = new csvCallbackReader ( false );
			r.readTerm ( new InputStreamReader ( new ByteArrayInputStream ( test[0].getBytes () ) ) );
			assertEquals ( test[1], r.fLastToken );
		}
	}

	private final String[][] terms = new String[][]
	{
		{ "test1,test2,test3", "test1" },
		{ "\"test1\",test2,test3", "test1" },
		{ "\"test1\na\",test2,test3", "test1\na" },
		{ "\"test1\"\"\",test2,test3", "test1\"" },
	};
}
