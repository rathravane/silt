package com.rathravane.till.data.json;

import org.json.JSONObject;
import org.junit.Test;

import com.rathravane.till.data.json.CommentedJsonTokener;

import junit.framework.TestCase;

public class CommentedJsonTokenerTest extends TestCase
{
	@Test
	public void testStripper ()
	{
		final JSONObject o = new JSONObject ( new CommentedJsonTokener ( "{'foo':'bar' /* bee */ }" ) );
		assertNotNull ( o );
		assertTrue ( o.has ( "foo" ) );
		assertEquals ( "bar", o.getString ( "foo" ) );
	}

	@Test
	public void testCommentInStringBody ()
	{
		final JSONObject o = new JSONObject ( new CommentedJsonTokener ( "{'foo':'bar /* bee */' }" ) );
		assertNotNull ( o );
		assertTrue ( o.has ( "foo" ) );
		assertEquals ( "bar /* bee */", o.getString ( "foo" ) );
	}

	@Test
	public void testMultilineBlockComment ()
	{
		final JSONObject o = new JSONObject ( new CommentedJsonTokener ( "{'foo':'bar', /* bee\nbar\n// blue */ 'bee':'baz' }" ) );
		assertNotNull ( o );
		assertEquals ( "bar", o.getString ( "foo" ) );
		assertEquals ( "baz", o.getString ( "bee" ) );
	}
}
