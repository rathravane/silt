package com.rathravane.till.data.json;

import org.json.JSONObject;
import org.junit.Test;

import junit.framework.TestCase;

public class JsonCloneTest extends TestCase
{
	@Test
	public void testClone ()
	{
		final JSONObject o = new JSONObject ()
			.put ( "foo", "bar" )
			.put ( "one", 1 )
			.put ( "sub", new JSONObject ().put ( "blat", "flat" ) )
		;
		final JSONObject that = JsonUtil.clone ( o );
		assertFalse ( that == o );
		assertEquals ( "flat", that.getJSONObject ( "sub" ).getString ( "blat" ) );
		o.getJSONObject ( "sub" ).put ( "blat", "rat" );
		assertEquals ( "flat", that.getJSONObject ( "sub" ).getString ( "blat" ) );
	}
}
