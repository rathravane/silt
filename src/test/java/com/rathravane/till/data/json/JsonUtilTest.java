package com.rathravane.till.data.json;

import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import com.rathravane.till.data.json.JsonVisitor.ArrayOfObjectVisitor;

import junit.framework.TestCase;

public class JsonUtilTest extends TestCase
{
	@Test
	public void testReadStringArray ()
	{
		final JSONObject base = new JSONObject ()
			.put ( "str", "string" )
			.put ( "arr", new JSONArray ().put ( "one" ).put ( "two" ).put ( "three" ) )
		;

		List<String> result = JsonUtil.readStringArray ( base, "str" );
		assertEquals ( 1, result.size () );
		assertEquals ( "string", result.iterator ().next () );
		
		result = JsonUtil.readStringArray ( base, "arr" );
		assertEquals ( 3, result.size () );
		
		final Iterator<String> it = result.iterator ();
		assertEquals ( "one", it.next () );
		assertEquals ( "two", it.next () );
		assertEquals ( "three", it.next () );
	}

	private static class IntegerHolder
	{
		public IntegerHolder ( int i ) { fValue = i; }
		public int getAndIncr () { return fValue++; }
		private int fValue;
	}

	@Test
	public void testArraySort ()
	{
		final JSONArray a = new JSONArray ()
			.put ( new JSONObject ().put ( "foo", 9 ) )
			.put ( new JSONObject ().put ( "foo", 6 ) )
			.put ( new JSONObject ().put ( "foo", 4 ) )
			.put ( new JSONObject ().put ( "foo", 3 ) )
			.put ( new JSONObject ().put ( "foo", 1 ) )
			.put ( new JSONObject ().put ( "foo", 2 ) )
			.put ( new JSONObject ().put ( "foo", 7 ) )
			.put ( new JSONObject ().put ( "foo", 5 ) )
			.put ( new JSONObject ().put ( "foo", 8 ) )
		;

		JsonUtil.sortArrayOfObjects ( a, "foo" );

		final IntegerHolder ih = new IntegerHolder ( 1 );
		JsonVisitor.forEachObjectIn ( a, new ArrayOfObjectVisitor() {

			@Override
			public boolean visit ( JSONObject t ) throws JSONException
			{
				assertEquals ( ih.getAndIncr(), t.getInt ( "foo" ) );
				return false;
			}
		} );
	}
}
