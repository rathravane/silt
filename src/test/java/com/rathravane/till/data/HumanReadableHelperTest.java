package com.rathravane.till.data;

import java.util.concurrent.TimeUnit;

import com.rathravane.till.time.clock;
import com.rathravane.till.time.clock.testClock;

import junit.framework.TestCase;

public class HumanReadableHelperTest extends TestCase
{
	private static final String kCurrencyTests [][]=
	{
		new String[] { "123.45", "123.45"  },
		new String[] { "-123.45", "-123.45"  },
		new String[] { "-1234.56", "-1,234.56"  },
		new String[] { "123.04", "123.04"  },
	};

	public void testDollarsAndCents ()
	{
		for ( String[] test : kCurrencyTests )
		{
			final double in = Double.parseDouble ( test[0] );
			final String out = humanReadableHelper.dollarValue ( in );
			assertEquals ( test[1], out );
		}
	}

	private static final String kTimeValueTests [][]=
	{
		new String[] { "12345", "1000", "12 s"  },
		new String[] { "12345", "60000", ""  },
	};

	public void testTimeValues ()
	{
		for ( String[] test : kTimeValueTests )
		{
			final long in = Long.parseLong ( test[0] );
			final long smallest = Long.parseLong ( test[1] );
			final String out = humanReadableHelper.timeValue ( in, TimeUnit.MILLISECONDS, smallest );
			assertEquals ( test[2], out );
		}
	}

	private static final String kElapsedTimeTests [][]=
	{
		new String[] { "1399999998000", "1000", "-1", "2 s ago"  },
		new String[] { "1399999998000", "60000", "-1", "just now"  },
		new String[] { "3", "0", "-1", "44 yrs, 4 months, 3 wks, 2 days, 16 hrs, 53 m, 19 s, 997 ms ago"  },
		new String[] { "3", "0", "2", "44 yrs, 4 months ago"  },
		new String[] { "1399911440000", "0", "2", "1 day ago"  },
	};

	public void testElapsedTimeValues ()
	{
		final testClock tc = clock.useNewTestClock ();
		tc.set ( 1400000000000L );

		for ( String[] test : kElapsedTimeTests )
		{
			final long in = Long.parseLong ( test[0] );
			final long smallest = Long.parseLong ( test[1] );
			final int levels = Integer.parseInt ( test[2] );
			final String out = humanReadableHelper.elapsedTimeSince ( in, smallest, levels );
			assertEquals ( test[3], out );
		}
	}
}
